import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RouterModule, Router } from '@angular/router';
import { Connect } from '../../connection';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  //ip info
  url_ip = Connect.url;

  token_id = localStorage.getItem('token');

  //myform
  myForm = [];
  scalar: String = "0";

  isNextPage = false;

  constructor(private http: HttpClient, private routes: Router) { }

  ngOnInit() {
    this.getForm();
    console.log(this.token_id);
  }

  getForm() {
    this.myForm = [];
    this.http.get(this.url_ip + '/forms', {
      headers: new HttpHeaders({
        'token': this.token_id,
        'start': "" + this.scalar
      })
    }).subscribe((data) => {
      console.log(data);

      var keys = Object.keys(data);
      var len = keys.length;

      if (len > 8) {
        this.isNextPage = true;
      }

      if (len > 0) {
        for (let i = 0; i < len; i++) {
          this.myForm.push({
            id: data[i]['id'],
            user_id: data[i]['user_id'],
            title: data[i]['title'],
            description: data[i]['description'],
            name: data[i]['name'],
            image: this.url_ip + data[i]['image'],
            question: data[i]['question'],
            share: data[i]['share'],
          })
          this.scalar = data[i]['form_id'];
        }
      }


    });
  }





}
