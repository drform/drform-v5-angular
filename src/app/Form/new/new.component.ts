import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Connect } from '../../connection';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})

export class NewComponent implements OnInit {


  url_ip = Connect.url;

  token_id = localStorage.getItem('token');

  //form
  nameForms: any;
  disForms: any;

  //image form
  formImage = [
    {
      url: "/assets/img/syringe.png",
      type: "image/png"
    }
  ];
  image_name_show: any = "Choose image";

  selectedFiles: File;

  //additem
  isAdditem: boolean = false;
  //item list
  myFormlist: number = 0;
  myForm = [];

  //type
  default_type = "String";
  type_id = [];
  index: any;

  //date
  typeDate = "Date";
  typeDatetime = "Datetime";
  //Textbox
  typeString = "String";
  typeText = "Text";
  typeInt = "Integer";
  typeFloat = "Float";
  //checkbox
  typeCheck = "Checkbox";
  //image
  typeFile = "File";
  //dropdown
  typeDropdown = "Dropdown";


  //checkbox test
  checkTest = [];




  constructor(private http: HttpClient, private modalService: NgbModal, private routes: Router) { }

  ngOnInit() {

  }

  addItem() {
    this.myFormlist += 1;
    if (this.myFormlist != 0) {
      this.isAdditem = true;
      this.myForm.push({
        order_no: this.myFormlist,
        data_type: "",
        title: "",
        description: "",
        required: false,
        av_search: false,
      });

    }
    console.log(this.myForm)
  }
  removeItem(index) {
    console.log(index);
    if (this.myFormlist > 0) {

      this.myFormlist -= 1;
      this.myForm.splice(index, 1);
    }

  }

  handleFileInput(event) {
    this.formImage = [];
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = <File>event.target.files[i];

      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.formImage.push({
          url: event.target.result,
          type: this.selectedFiles.type
        });
        this.image_name_show = this.selectedFiles.name;
      }

      reader.readAsDataURL(this.selectedFiles);

    }
  }

  createForm() {
    //base64 image
    var imageText: any;
    let itemArray: any;

    for (let item of this.formImage) {
      if (item.type === "image/jpeg") {
        imageText = item.url.split(",")[1];
      }
      else if (item.type === "image/png") {
        imageText = item.url.split(",")[1];
      }
      else if (item.type === "video/mp4") {
        imageText = item.url.split(",")[1];
      }
      else { //null
        imageText = "";
      }

      itemArray = imageText;
      // console.log("itemArray = " + itemArray);
    }

    let form_item: any = [];
    //formlist
    for (let item of this.myForm) {
      //console.log(item);
      form_item.push(item);
    }

    let myForm = {
      form: {
        title: this.nameForms,
        description: this.disForms,
        image: itemArray,
      },
      form_item
    }
    //console.log(myForm);
    this.http.post(this.url_ip + '/forms', myForm, {
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data) => {
      console.log(data);
      // let snackBarRef = snackBar.open('Message archived', 'Undo');

    });
  }

  selectType(type, index_list) {
    console.log(type);
    this.index = index_list;
    console.log(this.index);

    if (type == "Date") {
      this.type_id[this.index] = 1;
      this.myForm[this.index]['data_type'] = type;
    }
    else if (type == "Datetime") {
      this.type_id[this.index] = 2;
      this.myForm[this.index]['data_type'] = type;
    }
    else if (type == "String") {
      this.type_id[this.index] = 3;
      this.myForm[this.index]['data_type'] = type;
    }

    else if (type == "Text") {
      this.type_id[this.index] = 4;
      this.myForm[this.index]['data_type'] = type;
    }
    else if (type == "Integer") {
      this.type_id[this.index] = 5;
      this.myForm[this.index]['data_type'] = type;
    }
    else if (type == "Float") {
      this.type_id[this.index] = 6;
      this.myForm[this.index]['data_type'] = type;
    }
    else if (type == "Checkbox") {
      this.type_id[this.index] = 7;
      this.myForm[this.index]['data_type'] = type;
    }
    else if (type == "File") {
      this.type_id[this.index] = 8;
      this.myForm[this.index]['data_type'] = type;
    }
    else if (type == "Dropdown") {
      this.type_id[this.index] = 9;
      this.myForm[this.index]['data_type'] = type;
    }
  }

  preView() {
    //base64 image
    var imageText: any;
    let itemArray: any;

    for (let item of this.formImage) {
      if (item.type === "image/jpeg") {
        imageText = item.url.split(",")[1];
      }
      else if (item.type === "image/png") {
        imageText = item.url.split(",")[1];
      }
      else if (item.type === "video/mp4") {
        imageText = item.url.split(",")[1];
      }
      else { //null
        imageText = "";
      }

      itemArray = imageText;
      // console.log("itemArray = " + itemArray);
    }
    let preform: any = [];
    console.log(this.myForm);
    for (let item of this.myForm) {
      preform.push(
        {
          av_search: item.av_search,
          data_type: item.data_type,
          data_type_id: this.type_id[item.order_no - 1],
          description: item.description,
          dropdown: "",
          form_id: "",
          id: "",
          order_no: item.order_no,
          required: item.required,
          title: item.title,
        }
      );
    }

    let question: any = [];
    //formlist
    for (let item of preform) {

      question.push(item);
    }


    let previewForm = {
      form: {
        id: "",
        user_id: "",
        name: "",
        title: this.nameForms,
        description: this.disForms,
        image: itemArray,
        question
      },
    }

    console.log(previewForm);
    localStorage.setItem('preview', JSON.stringify(previewForm));
    this.routes.navigate(['/contents/preview']);

  }

  drop(event: CdkDragDrop<string[]>) {//2,0
    moveItemInArray(this.myForm, event.previousIndex, event.currentIndex); //before , after index
    console.log(event.previousIndex, event.currentIndex);

    //change order

    console.log(this.myForm[event.currentIndex]['order_no']);
    for (let i = 0; i < this.myForm.length; i++) {
      this.myForm[i]['order_no'] = i + 1;
    }



  }


}


