import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowRoutingModule } from './show-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { ShowComponent } from '../../Form/show/show.component';
import { AnswerComponent } from '../show/answer/answer.component';
import { DetailComponent } from '../../Form/show/detail/detail.component';

@NgModule({

  imports: [
    CommonModule,
    ShowRoutingModule,
    FormsModule,
    NgbModule
  ],
  declarations: [
    ShowComponent,
    AnswerComponent,
    DetailComponent
  ],
})
export class ShowModule { }
