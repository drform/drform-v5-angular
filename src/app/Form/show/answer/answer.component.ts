import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Connect } from '../../../connection';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {

  //ip info
  url_ip = Connect.url;

  token_id = localStorage.getItem('token');
  form_id = +this.route.snapshot.paramMap.get('id');

  //myform
  myForm = [];

  myQuestion = [];
  myType = [];
  myAnswer = [];
  myTest = [];

  //question
  isDate: any = [];
  isDatetime: any = [];

  isString: any = [];
  isText: any = [];
  isInt: any = [];
  isFloat: any = [];

  isCheck: any = [];
  isFile: any = [];
  isDropdown: any = [];

  //input text
  answerImage = [];
  selectedFiles: File;

  constructor(private route: ActivatedRoute, private http: HttpClient, public datepipe: DatePipe) { }

  ngOnInit() {

    this.getForm();
  }

  getForm() {
    const id = +this.route.snapshot.paramMap.get('id');

    this.http.get(this.url_ip + '/forms/' + id, {
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data: any) => {
      console.log(data);
      var keys = Object.keys(data);
      var len = keys.length;

      if (len > 0) {
        this.myForm.push({
          id: data['form']['id'],
          title: data['form']['title'],
          description: data['form']['description'],
          name: data['form']['name'],
          image: this.url_ip + data['form']['image'],
          question: data['form']['question'],
          share: data['form']['share'],
        })


        var keys = Object.keys(this.myForm[0]['question']);
        var len = keys.length;

        for (let i = 0; i < len; i++) {
          this.myQuestion.push({
            id: this.myForm[0]['question'][i].id,
            form_id: this.myForm[0]['question'][i].form_id,
            order_no: this.myForm[0]['question'][i].order_no,
            title: this.myForm[0]['question'][i].title,
            description: this.myForm[0]['question'][i].description,
            data_type_id: this.myForm[0]['question'][i].data_type_id,
            data_type: this.myForm[0]['question'][i].data_type,
            dropdown: this.myForm[0]['question'][i].dropdown,
            required: this.myForm[0]['question'][i].required,
            av_search: this.myForm[0]['question'][i].av_search,
            value: "",
          })
        }

        //console.log(this.myQuestion);

      }

      var keys = Object.keys(data['answer']);
      var lenAns = keys.length;
      if (lenAns > 0) {
        this.myAnswer.push({
          id: data['answer']['id'],
          user_id: data['answer']['user_id'],
          form_id: data['answer']['form_id'],
          answer_item: [
            {
              id: data['answer']['answer_item'],
              user_form_id: "",
              form_item_id: "",
              date_value: "",
              datetime_value: "",
              string_value: "",
              text_value: "",
              integer_value: "",
              float_value: "",
              checkbox_value: "",
              file_value: "",
              dropdown_value_id: "",
            }
          ]

        })
      }

      this.getType();

    });
  }

  getType() {

    this.http.get(this.url_ip + '/data_types', {
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data: any) => {


      var keys = Object.keys(this.myForm[0]['question']); //size data type
      var len = keys.length;

      //find Typequestion == data type
      let num = 0;


      for (let i = 0; i < len; i++) {


        this.myTest.push(this.myForm[0]['question'][i]);
        let order = this.myForm[0]['question'][i].order_no;
        let typeId = this.myForm[0]['question'][i].data_type_id;
        this.createFormType(order, typeId);

        // if (typeId == data[i]['id']) {
        //   this.myType.push(typeId);
        //   this.createFormType(num);
        //   num++;
        // }

      }
      //console.log(this.myTest);


    });
  }

  createFormType(order, typeId) {


    //console.log(this.myType[i]);
    if (typeId == 1) { //Date
      this.isDate[order - 1] = true;

    }
    else if (typeId == 2) { //Datetime
      this.isDatetime[order - 1] = true;

    }
    else if (typeId == 3) { //String
      this.isString[order - 1] = true;

    }
    else if (typeId == 4) { //Text
      this.isText[order - 1] = true;

    }
    else if (typeId == 5) { //Integer
      this.isInt[order - 1] = true;

    }
    else if (typeId == 6) { //Float
      this.isFloat[order - 1] = true;

    }
    else if (typeId == 7) { //Checkbox
      this.isCheck[order - 1] = true;
      this.myQuestion[order - 1]['value'] = false; //set default

    }
    else if (typeId == 8) { //Image
      this.isFile[order - 1] = true;

    }
    else if (typeId == 9) { //Dropdown
      this.isDropdown[order - 1] = true;

    }
  }

  submit() {
    let itemArray: any;
    if (this.isFile == true) {
      //base64 image
      var imageText: any;


      for (let item of this.answerImage) {
        if (item.type === "image/jpeg") {
          imageText = item.url.split(",")[1];
        }
        else if (item.type === "image/png") {
          imageText = item.url.split(",")[1];
        }
        else if (item.type === "video/mp4") {
          imageText = item.url.split(",")[1];
        }
        else { //null
          imageText = "";
        }
        itemArray = imageText;
      }
    }

    var keys = Object.keys(this.myForm[0]['question']);
    var len = keys.length;

    let user_form_item = [];

    for (let i = 0; i < len; i++) {
      //console.log(this.myQuestion[i]['value']);
      // let latest_date = this.datepipe.transform(this.myQuestion[i]['value'], 'yyyy-MM-ddTHH:mm:ss');
      user_form_item.push({
        order_no: (i + 1),
        value: this.myQuestion[i]['value']
      })

    }

    let answer = {
      form_id: this.form_id,
      user_form_item
    }

    this.http.post(this.url_ip + '/user_forms/', answer, {
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data) => {
      console.log(data);

    });
  }

  handleFileInput(event, index) {
    //this.answerImage = [];
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = <File>event.target.files[i];

      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.answerImage.push({
          url: event.target.result,
          type: this.selectedFiles.type
        });
        //this.image_name_show = this.selectedFiles.name;
        let itemArray: any = [];
        var imageText: any;
        for (let item of this.answerImage) {
          if (item.type === "image/jpeg") {
            imageText = item.url.split(",")[1];
          }
          else if (item.type === "image/png") {
            imageText = item.url.split(",")[1];
          }
          else if (item.type === "video/mp4") {
            imageText = item.url.split(",")[1];
          }
          else { //null
            imageText = "";
          }
          //console.log(imageText);
          itemArray.push({
            url: imageText
          })
          console.log(itemArray);
          this.myQuestion[index]['value'] = itemArray;
          console.log(this.myQuestion[index]['value']);
        }
      }

      reader.readAsDataURL(this.selectedFiles);

      console.log(this.answerImage);

    }
  }

  previewAns(form) {
    console.log("preview");
  }





}
