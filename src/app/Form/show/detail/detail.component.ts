import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Connect } from '../../../connection';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RouterModule, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  //ip info
  url_ip = Connect.url;

  token_id = localStorage.getItem('token');
  form_id: any;

  myAnswer = [];

  constructor(private route: ActivatedRoute, private http: HttpClient, private routes: Router, private location: Location) { }

  ngOnInit() {
    this.getDetail();
  }


  getDetail() {
    const id = +this.route.snapshot.paramMap.get('id');
    console.log(id);

    this.http.get(this.url_ip + '/user_forms/' + id + '?user_form_id=' + id, {
      headers: new HttpHeaders({
        'token': this.token_id,
      }),

    }).subscribe((data: any) => {
      console.log(data);
      this.myAnswer.push(data);
      this.form_id = data['form_id'];
      //console.log(this.myAnswer);
    });



  }


  backPage() {
    this.location.back();
  }

}
