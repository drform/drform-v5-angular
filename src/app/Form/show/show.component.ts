import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Connect } from '../../connection';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RouterModule, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  //ip info
  url_ip = Connect.url;

  token_id = localStorage.getItem('token');

  //myform
  myForm = [];
  myAnswer = [];
  AnswerTitle = [];

  //ansForm
  isAnsForm: boolean = false;

  constructor(private route: ActivatedRoute, private http: HttpClient, private routes: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.getForm();
  }

  getForm() {
    const id = +this.route.snapshot.paramMap.get('id');

    this.http.get(this.url_ip + '/forms/' + id, {
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data: any) => {
      console.log(data);
      var keys = Object.keys(data);
      var len = keys.length;

      if (len > 0) {
        this.myForm.push({
          id: data['form']['id'],
          title: data['form']['title'],
          description: data['form']['description'],
          name: data['form']['name'],
          image: this.url_ip + data['form']['image'],
          question: data['form']['question'],
          share: data['form']['share'],
        })
      }

      var keys = Object.keys(data['answer']);
      var lenAns = keys.length;
      if (lenAns > 0) {

        for (let i = 0; i < lenAns; i++) {
          this.AnswerTitle.push(data['answer'][i]);
        }

        //console.log(this.AnswerTitle);


      }

    });
  }

  deleteForm() {
    if (confirm("Are you sure delete?")) {
      const id = +this.route.snapshot.paramMap.get('id');

      this.http.delete(this.url_ip + '/forms/' + id, {
        headers: new HttpHeaders({
          'token': this.token_id,
        })
      }).subscribe((data: any) => {
        console.log("Deleted");
      });
    }



  }

  shareForm() {

  }

}
