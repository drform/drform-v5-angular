import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowComponent } from '../../Form/show/show.component';
import { AnswerComponent } from '../show/answer/answer.component';
import { DetailComponent } from '../../Form/show/detail/detail.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ShowComponent
      },
      { path: 'answer', component: AnswerComponent },
      { path: 'details/:id', component: DetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowRoutingModule { }
