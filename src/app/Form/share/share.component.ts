import { Component, OnInit } from '@angular/core';
import { Connect } from '../../connection';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.css']
})
export class ShareComponent implements OnInit {

  //ip info
  url_ip = Connect.url;

  token_id = localStorage.getItem('token');

  //myform
  shareForm = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getShareform();
  }

  getShareform() {
    this.http.get(this.url_ip + '/sharewithme', {
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data: any) => {
      console.log(data);

      var keys = Object.keys(data);
      var len = keys.length;
      if (len > 0) {
        for (let i = 0; i < len; i++) {
          this.shareForm.push({
            id: data[i]['id'],
            form_id: data[i]['form_id'],
            title: data[i]['title'],
            user_id: data[i]['user_id'],
            image: this.url_ip + data[i]['image'],
            share_role: data[i]['share_role'],
          })
        }

      }

      console.log(this.shareForm);


    })

  }

}
