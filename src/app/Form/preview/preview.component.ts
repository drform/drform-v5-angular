import { Component, OnInit } from '@angular/core';
import { Connect } from './../../connection';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {

  //ip info
  url_ip = Connect.url;

  preForm = localStorage.getItem('preview');
  token_id = localStorage.getItem('token');

  myForm = [];
  myQuestion = [];
  myAnswer = [];
  myTest = [];

  //question
  isDate: any = [];
  isDatetime: any = [];

  isString: any = [];
  isText: any = [];
  isInt: any = [];
  isFloat: any = [];

  isCheck: any = [];
  isFile: any = [];
  isDropdown: any = [];

  constructor() { }

  ngOnInit() {
    this.previewForm();
  }

  previewForm() {
    var data = JSON.parse(this.preForm);
    console.log(data);
    var keys = Object.keys(data);
    var len = keys.length;

    if (len > 0) {
      this.myForm.push({
        id: data['form']['id'],
        title: data['form']['title'],
        description: data['form']['description'],
        name: data['form']['name'],
        image: this.url_ip + data['form']['image'],
        question: data['form']['question'],
        share: data['form']['share'],
      })

      var keys = Object.keys(this.myForm[0]['question']);
      var len = keys.length;

      for (let i = 0; i < len; i++) {
        this.myQuestion.push({
          id: this.myForm[0]['question'][i].id,
          form_id: this.myForm[0]['question'][i].form_id,
          order_no: this.myForm[0]['question'][i].order_no,
          title: this.myForm[0]['question'][i].title,
          description: this.myForm[0]['question'][i].description,
          data_type_id: this.myForm[0]['question'][i].data_type_id,
          data_type: this.myForm[0]['question'][i].data_type,
          dropdown: this.myForm[0]['question'][i].dropdown,
          required: this.myForm[0]['question'][i].required,
          av_search: this.myForm[0]['question'][i].av_search,
          value: "",
        })
      }
    }


    this.getType();

  }

  getType() {
    var keys = Object.keys(this.myForm[0]['question']); //size data type
    var len = keys.length;

    //find Typequestion == data type
    let num = 0;

    for (let i = 0; i < len; i++) {
      this.myTest.push(this.myForm[0]['question'][i]);
      let order = this.myForm[0]['question'][i].order_no;
      let typeId = this.myForm[0]['question'][i].data_type_id;
      let typeName = this.myForm[0]['question'][i].data_type;
      console.log(typeName, order, typeId);
      this.createFormType(order, typeName);
    }
  }

  createFormType(order, typeName) {


    //console.log(this.myType[i]);
    if (typeName == "Date") { //Date
      this.isDate[order - 1] = true;

    }
    else if (typeName == "Datetime") { //Datetime
      this.isDatetime[order - 1] = true;

    }
    else if (typeName == "String") { //String
      this.isString[order - 1] = true;

    }
    else if (typeName == "Text") { //Text
      this.isText[order - 1] = true;

    }
    else if (typeName == "Integer") { //Integer
      this.isInt[order - 1] = true;

    }
    else if (typeName == "Float") { //Float
      this.isFloat[order - 1] = true;

    }
    else if (typeName == "Checkbox") { //Checkbox
      this.isCheck[order - 1] = true;
      this.myQuestion[order - 1]['value'] = false; //set default

    }
    else if (typeName == "File") { //Image
      this.isFile[order - 1] = true;

    }
    else if (typeName == "Dropdown") { //Dropdown
      this.isDropdown[order - 1] = true;

    }
  }

}