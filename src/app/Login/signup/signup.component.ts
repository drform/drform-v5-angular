import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Connect } from '../../connection';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  url_ip = Connect.url;
  //image form
  formImage = [
    {
      name: "doctor.png",
      path: "/assets/img/doctor.png",
      type: "base64"
    }
  ];
  imageName: any = "Choose image";
  selectedFiles: File;

  alertMeg: String = "";

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  signUp(user, pass, confirmpass, email) {
    if (user == "" || pass == "" || confirmpass == "" || email == "") {
      this.alertMeg = "some text is empty."
    }
    else if (pass != confirmpass) {
      this.alertMeg = "password is not match."
    } else {

      var register = {
        user: {
          username: user,
          password: pass,
          name: user,
          email: email,
          tel: '0891124455',
          position: "your position",
          image: "your image"
        }
      }

      console.log(register);

      this.http.post(this.url_ip + "/users", register, {
        headers: new HttpHeaders({
          'type': "base64",
        })
      }).subscribe((data) => {
        console.log(data);
      });
    }
  }

  handleFileInput(event) {
    this.formImage = [];
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = <File>event.target.files[i];

      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.formImage.push({
          name: this.selectedFiles.name,
          path: event.target.result,
          type: this.selectedFiles.type
        });
        this.imageName = this.selectedFiles.name
      }
      reader.readAsDataURL(this.selectedFiles);

    }
  }



}
