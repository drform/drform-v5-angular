import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';
import { Connect } from '../../connection';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  //ip info
  url_ip = Connect.url;

  constructor(private routes: Router, private Auth: AuthService) { }

  ngOnInit() {
  }

  signIn(username, password) {
    this.Auth.getUserDetails(username, password).subscribe(data => {
      console.log(data);
      data.success = true;
      if (data.success) {

        var image_url = this.url_ip + data['user_info']['image'].url;
        localStorage.setItem('id', data['user_info']['id']);
        localStorage.setItem('user_name', data['user_info']['username']);
        localStorage.setItem('name', data['user_info']['name']);
        localStorage.setItem('email', data['user_info']['email']);
        localStorage.setItem('position', data['user_info']['position']);
        localStorage.setItem('tel', data['user_info']['tel']);
        localStorage.setItem('role_id', data['user_info']['role_id']);
        localStorage.setItem('image_url', image_url);
        localStorage.setItem('image_type', data['user_info']['image'].content_type);
        localStorage.setItem('token', data['token']);
        this.Auth.setSignin(true);
        this.routes.navigate(['/forms']);

      } else {
        data.message = "username or password is wrong.";
        window.alert(data.message);
      }

    })
    console.log(username, password);

  }

}
