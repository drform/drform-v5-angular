import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from '../../User/user/user.component';
import { EdituserComponent } from '../../User/user/edituser/edituser.component';
import { ShowuserComponent } from '../../User/user/showuser/showuser.component';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      { path: 'show', component: ShowuserComponent },
      { path: 'edit', component: EdituserComponent },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
