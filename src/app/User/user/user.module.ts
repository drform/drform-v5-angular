import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { EdituserComponent } from '../../User/user/edituser/edituser.component';
import { UserComponent } from '../../User/user/user.component';
import { ShowuserComponent } from '../../User/user/showuser/showuser.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule
  ],
  declarations: [
    UserComponent,
    EdituserComponent,
    ShowuserComponent
  ],
})
export class UserModule { }
