import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showuser',
  templateUrl: './showuser.component.html',
  styleUrls: ['./showuser.component.css']
})
export class ShowuserComponent implements OnInit {

  user_id = localStorage.getItem('id');
  user_name = localStorage.getItem('user_name');
  name = localStorage.getItem('name');
  email = localStorage.getItem('email');
  position = localStorage.getItem('position');
  tel = localStorage.getItem('tel');
  role_id = localStorage.getItem('role_id');
  image_url = localStorage.getItem('image_url');
  image_type = localStorage.getItem('image_type');
  token = localStorage.getItem('token');

  constructor() { }

  ngOnInit() {
  }

}
