import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContentRoutingModule } from './content-routing.module';
import { ContentComponent } from '../content/content.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { FooterComponent } from '../footer/footer.component';
import { NewComponent } from '../../Form/new/new.component';
import { EditComponent } from '../../Form/edit/edit.component';
import { ListComponent } from '../../Form/list/list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ShareComponent } from '../../Form/share/share.component';
import { PreviewComponent } from '../../Form/preview/preview.component';

import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule,
    ContentRoutingModule,
    FormsModule,
    NgbModule,
    DragDropModule,
  ],
  declarations: [
    ContentComponent,
    NavbarComponent,
    FooterComponent,
    NewComponent,
    EditComponent,
    ListComponent,
    ShareComponent,
    PreviewComponent,
  ],
})
export class ContentModule { }
