import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from '../content/content.component';

import { NewComponent } from '../../Form/new/new.component';
import { EditComponent } from '../../Form/edit/edit.component';

import { ListComponent } from '../../Form/list/list.component';
import { ShareComponent } from '../../Form/share/share.component';

import { PreviewComponent } from '../../Form/preview/preview.component'


const routes: Routes = [
  {
    path: '',
    component: ContentComponent,
    children: [
      { path: 'new', component: NewComponent },
      { path: 'edit', component: EditComponent },
      { path: 'preview', component: PreviewComponent },
      {
        path: 'shows/:id',
        loadChildren: 'src/app/Form/show/show.module#ShowModule',
      },
      { path: 'forms', component: ListComponent },
      { path: 'shareforms', component: ShareComponent },
      {
        path: 'users',
        loadChildren: 'src/app/User/user/user.module#UserModule',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule { }
