import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Connect } from '../../connection';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  //ip info
  url_ip = Connect.url;

  //user info
  user_username = localStorage.getItem('user_name');
  user_name = localStorage.getItem('name');
  user_email = localStorage.getItem('email');
  user_image_url = localStorage.getItem('image_url');
  token_id = localStorage.getItem('token');

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  signOut() {
    this.http.delete(this.url_ip + "/logout", {
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data) => {
      console.log(data);

      localStorage.setItem('id', "");
      localStorage.setItem('user_name', "");
      localStorage.setItem('name', "");
      localStorage.setItem('email', "");
      localStorage.setItem('position', "");
      localStorage.setItem('tel', "");
      localStorage.setItem('role_id', "");
      localStorage.setItem('image_url', "");
      localStorage.setItem('image_type', "");
      localStorage.setItem('token', "");


    });
  }

}
