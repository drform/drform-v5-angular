import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Connect } from './connection';

interface myData {
  success: boolean,
  message: String
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //ip info
  url_ip = Connect.url;

  private signinStatus = false;

  constructor(private http: HttpClient) { }

  setSignin(value: boolean) {
    this.signinStatus = value;
  }

  getUserDetails(user, pass) {
    var login = {
      session: {
        username: user,
        password: pass
      }
    }
    return this.http.post<myData>(this.url_ip + "/login", login, {
    })
  }

}
